#ifndef __LIBRARIES_H__
#define __LIBRARIES_H__

BEGIN_GNOME_DECLS


typedef struct _GbfLibrary GbfLibrary;

struct _GbfLibrary {
    char *name;
    char *description
    char *language;

    GSList *deps;
};

GbfLibrary      *gbf_library_new    (char   *name,
				     char   *description,
				     char   *language,
				     GSList *deps);

END_GNOME_DECLS
#endif /* __LIBRARIES_H__ */
