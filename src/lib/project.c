#include <bonobo/bonobo-object.h>
#include "project.h"
#include "target.h"
#include "line.h"

static GtkObjectClass *gbf_project_parent_class;

static CORBA_Object create_gbf_project(BonoboObject * object);
static void project_destroy(GtkObject * object);
static void project_class_init(GbfProjectClass * class);
static void project_init(GbfProject * project);

static Bonobo_stringlist *impl__get_target_list(PortableServer_Servant
					     servant,
					     CORBA_Environment * ev);
static GBF_Target impl_get_target(PortableServer_Servant servant,
				  const CORBA_char * name,
				  CORBA_Environment * ev);
static void impl_remove_target(PortableServer_Servant servant,
			       const CORBA_char * name, 
			       CORBA_Environment * ev);
static GBF_Target impl_create_target(PortableServer_Servant servant,
				     const CORBA_char * name,
				     GBF_Target_Type type,
				     CORBA_Environment * ev);



static POA_GBF_Project__vepv gbf_project_vepv;

GtkType gbf_project_get_type(void)
{
    static GtkType type = 0;

    if (!type) {
	GtkTypeInfo info = {
	    "IDL:GBF/Project:1.0",
	    sizeof(GbfProject),
	    sizeof(GbfProjectClass),
	    (GtkClassInitFunc) project_class_init,
	    (GtkObjectInitFunc) project_init,
	    NULL, NULL,
	};
	type = gtk_type_unique(bonobo_object_get_type(), &info);
    }
    return type;
}

GbfProject *gbf_project_new(char * path)
{
    GBF_Project corba_project;
    GbfProject *project;

    project = gtk_type_new(gbf_project_get_type());
    corba_project =
	(GBF_Project) create_gbf_project(BONOBO_OBJECT(project));
    if (corba_project == CORBA_OBJECT_NIL) {
	gtk_object_destroy(GTK_OBJECT(project));
	return NULL;
    }
    bonobo_object_construct(BONOBO_OBJECT(project), corba_project);

    project->path = path;
    return project;
}

void gbf_project_add_child(GbfProject * project, GbfProject * child)
{
    project->subprojects = g_slist_append(project->subprojects, child);
    child->parent = project;
}

void gbf_project_add_file(GbfProject * project, char * file)
{
    project->processed_files = g_slist_append(project->processed_files,
					      g_strdup(file));
}

void
gbf_project_add_subst(GbfProject * project, char * name, char * value)
{
    char *storename, *storevalue;

    if (!g_hash_table_lookup(project->vars, name)) {
	storename = g_strdup(name);
	storevalue = g_strdup(value);
	project->inserthere = gbf_line_create (project->inserthere,
					       GBF_LINE_SUBST,
					       storename, storevalue);
	g_hash_table_insert(project->automakevars, storename, project->inserthere);
    }
}

void gbf_project_add_var(GbfProject * project, char * name, char * value)
{
    char *storename, *storevalue;
    gpointer a, b;

    storename = g_strdup(name);
    storevalue = g_strdup(value);
    project->inserthere = gbf_line_create(project->inserthere,
					  GBF_LINE_VAR,
					  storename, storevalue);
    if (!g_hash_table_lookup_extended(project->vars, name, &a, &b)) {
	g_hash_table_insert(project->vars, storename, project->inserthere);
    }
}

POA_GBF_Project__epv *gbf_project_get_epv(void)
{
    POA_GBF_Project__epv *epv;

    epv = g_new0(POA_GBF_Project__epv, 1);
    epv->_get_target_list = impl__get_target_list;
    epv->get_target = impl_get_target;
    epv->remove_target = impl_remove_target;
    epv->create_target = impl_create_target;

    return epv;
}

static void init_project_corba_class(void)
{
    gbf_project_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv();
    gbf_project_vepv.GBF_Project_epv = gbf_project_get_epv();
}

static void add_target(GbfTarget * target, GbfProject * project)
{
    g_hash_table_insert(project->targets, target->name, target);
}

void gbf_project_add_target(GbfProject * project, GList * targets)
{
    g_list_foreach(targets, (GFunc) add_target, project);
}

static void remove_target(GbfTarget * target, GbfProject * project)
{
}

void gbf_project_remove_target(GbfProject * project, GList * targets)
{
    g_list_foreach(targets, (GFunc) remove_target, project);
}

static Bonobo_stringlist *impl__get_target_list(PortableServer_Servant
					     servant,
					     CORBA_Environment * ev)
{
   Bonobo_stringlist *retval = NULL;

   return retval;
}

static GBF_Target
impl_get_target(PortableServer_Servant servant,
		const CORBA_char * name, CORBA_Environment * ev)
{
   GBF_Target retval = CORBA_OBJECT_NIL;

   return retval;
}

static void
impl_remove_target(PortableServer_Servant servant,
		   const CORBA_char * name, CORBA_Environment * ev)
{
}

static GBF_Target
impl_create_target(PortableServer_Servant servant,
		   const CORBA_char * name, 
		   GBF_Target_Type type,
		   CORBA_Environment * ev)
{
   GBF_Target retval = CORBA_OBJECT_NIL;

   return retval;
}

static CORBA_Object create_gbf_project(BonoboObject * object)
{
    POA_GBF_Project *servant;
    CORBA_Environment ev;

    CORBA_exception_init(&ev);
    servant = (POA_GBF_Project *) g_new0(GbfProject, 1);
    servant->vepv = &gbf_project_vepv;

    POA_GBF_Project__init((PortableServer_Servant) servant, &ev);

    if (ev._major != CORBA_NO_EXCEPTION) {
	g_free(servant);
	CORBA_exception_free(&ev);
	return CORBA_OBJECT_NIL;
    }
    CORBA_exception_free(&ev);
    return bonobo_object_activate_servant(object, servant);
}

static void project_destroy(GtkObject * object)
{
//    GbfProject *project = GBF_PROJECT(object);

    GTK_OBJECT_CLASS(gbf_project_parent_class)->destroy(object);
}

static void project_class_init(GbfProjectClass * klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    gbf_project_parent_class = gtk_type_class(bonobo_object_get_type());

    /* FIXME: setup signals */
    object_class->destroy = project_destroy;
    init_project_corba_class();
}

static void project_init(GbfProject * project)
{
    /* FIXME: actually do init stuff here */
}
