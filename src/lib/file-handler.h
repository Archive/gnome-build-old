#ifndef __FILE_HANDLER_H__
#define __FILE_HANDLER_H__

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>
#include "gbf-file-handler.h"
#include "types.h"

BEGIN_GNOME_DECLS

#define GBF_FILE_HANDLER_TYPE        (gbf_file_handler_get_type ())
#define GBF_FILE_HANDLER(o)          (GTK_CHECK_CAST ((o), GBF_FILE_HANDLER_TYPE, GbfFileHandler))
#define GBF_FILE_HANDLER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GBF_FILE_HANDLER_TYPE, GbfFileHandlerClass))
#define GBF_IS_FILE_HANDLER(o)       (GTK_CHECK_TYPE ((o), GBF_FILE_HANDLER_TYPE))
#define GBF_IS_FILE_HANDLER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GBF_FILE_HANDLER_TYPE))


#define GBF_FILE_HANDLER_FACTORY(fn) ((GbfFileHandlerFactory)(fn))
typedef GtkWidget * (*GbfFileHandlerFactory) (void);

typedef struct _GbfFileHandler GbfFileHandler;
typedef struct _GbfFileHandlerClass GbfFileHandlerClass
;
typedef void (*GbfFileHandlerFunc) (GbfFileHandler *fh, gpointer user_data);

struct _GbfFileHandler {
	BonoboObject parent;

	GHashTable *verbs;
	GbfProject *project;
};

struct _GbfFileHandlerClass {
	BonoboObjectClass parent_class;

	void (*on_getverb) (GbfFileHandler *file_handler, char *file, GList *list);
	void (*on_doverb) (GbfFileHandler *file_handler, char *file, char *verb, gint *result);

};


GtkType gbf_file_handler_get_type  (void);

GbfFileHandler *gbf_file_handler_new (BonoboUIHandler * handler);
void gbf_file_handler_add_verb (GbfFileHandler * handler,
				char * verb,
				GbfFileHandlerFunc func);
void gbf_file_handler_remove_verb (GbfFileHandler * handler,
				   char * verb);


END_GNOME_DECLS
#endif /* __FILE_HANDLER_H__ */

