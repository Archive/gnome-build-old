#include <glib.h>
#include "line.h"

static GNode *add_line(GNode * parent, GbfLine * newline)
{
    switch (newline->lt) {
    case GBF_LINE_ELSE:
    case GBF_LINE_FI:
	return g_node_append_data(parent->parent, newline);
	break;
    default:
	return g_node_append_data(parent, newline);
	break;
    }
}


GNode *gbf_line_create(GNode * node, GbfLineType type, char * name,
		       gpointer data)
{
    GbfLine *current, *new;

    new = g_new0(GbfLine, 1);
    new->lt = type;
    new->name = name;
    new->value = data;

    current = node->data;

    /* handle toplevel case */
    if (!current) {
       add_line (node, new);
    }
    switch (current->lt) {
    case GBF_LINE_IF:
    case GBF_LINE_ELSE:
	return add_line(node, new);
	break;
    default:
	return add_line(node->parent, new);
	break;
    }
}
