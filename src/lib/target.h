#ifndef __TARGET_H__
#define __TARGET_H__

#include <bonobo/bonobo-object.h>
#include "types.h"
#include "gbf-target.h"

BEGIN_GNOME_DECLS

#define GBF_TARGET_TYPE        (gbf_target_get_type ())
#define GBF_TARGET(o)          (GTK_CHECK_CAST ((o), GBF_TARGET_TYPE, GBFTarget))
#define GBF_TARGET_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GBF_TARGET_TYPE, GBFTargetClass))
#define GBF_IS_TARGET(o)       (GTK_CHECK_TYPE ((o), GBF_TARGET_TYPE))
#define GBF_IS_TARGET_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GBF_TARGET_TYPE))

typedef enum {
    GBF_TITEM_FILE,
    GBF_TITEM_DEPENDENCY,
    GBF_TITEM_LDFLAGS,
    GBF_TITEM_LIBS
} GbfTargetItem;

struct _GbfTarget {
    BonoboObject parent;

    char *name;
    GBF_Target_Type type;
    char *prefix;
    GHashTable *sources;
    GSList *deps;
    GSList *linkflags;
    GSList *libs;

    GbfMakefile *makefile;
};

typedef struct {
    BonoboObjectClass parent_class;
    void (*added) (GbfTarget * target, GbfTargetItem what, gpointer item);

    void (*modified) (GbfTarget * target,
		      GbfTargetItem what, gpointer item);

    void (*removed) (GbfTarget * target,
		     GbfTargetItem what, gpointer item);


} GbfTargetClass;

GtkType gbf_target_get_type(void);

/* API for the parser */
GbfTarget *gbf_target_new   (char * name, char * type, char * prefix);
void gbf_target_add_file    (GbfTarget * target, char * name, gint flags);
void gbf_target_add_deps    (GbfTarget * target, GSList * flags);
void gbf_target_add_ldflags (GbfTarget * target, GSList * flags);
void gbf_target_add_libs    (GbfTarget * target, GSList * flags);

/* for GbfMakefile only */
void gbf_target_set_makefile     (GbfTarget * target, GbfMakefile * makefile);
const char *gbf_target_get_name (GbfTarget * target);

END_GNOME_DECLS
#endif				/* __TARGET_H__ */
