#ifndef __TYPES_H__
#define __TYPES_H__

typedef struct _GbfParser	GbfParser;
typedef struct _GbfParserClient GbfParserClient;
typedef struct _GbfProject	GbfProject;
typedef struct _GbfMakefile	GbfMakefile;
typedef struct _GbfTarget	GbfTarget;

#endif // __TYPES_H__ */
