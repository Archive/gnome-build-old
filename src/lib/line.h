#ifndef __LINE_H__
#define __LINE_H__

typedef enum {
    GBF_LINE_IF,
    GBF_LINE_ELSE,
    GBF_LINE_FI,
    GBF_LINE_MACRO,
    GBF_LINE_VAR,
    GBF_LINE_COMMENT,
    GBF_LINE_TARGET,
    GBF_LINE_TARGETLIST,
    GBF_LINE_TARGET_DATA_LIST,
    GBF_LINE_TARGET_DATA_HASH,
    GBF_LINE_RULE,
    GBF_LINE_OTHER,
    GBF_LINE_SUBST,
    GBF_LINE_WHITESPACE
} GbfLineType;

typedef struct {
    GbfLineType lt;
    char *name;
    gpointer value;
} GbfLine;

GNode *gbf_line_create(GNode * node,
		       GbfLineType type, char * name, gpointer data);

#endif
