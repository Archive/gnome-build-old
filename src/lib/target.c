#include <bonobo/bonobo-object.h>

#include "gbf-target.h"
#include "makefile.h"
#include "target.h"

static GtkObjectClass *gbf_target_parent_class;


static CORBA_Object create_gbf_target(BonoboObject * object);
static void target_destroy(GtkObject * object);
static void target_class_init(GbfTargetClass * klass);
static void target_init(GbfTarget * target);

static GBF_Target_Type impl__get_target_type(PortableServer_Servant
					     servant,
					     CORBA_Environment * ev);
static CORBA_char *impl__get_name(PortableServer_Servant servant,
				  CORBA_Environment * ev);
static void impl__set_name(PortableServer_Servant servant,
			   const CORBA_char * name, CORBA_Environment * ev);
static CORBA_char *impl__get_prefix(PortableServer_Servant servant,
				    CORBA_Environment * ev);
static void impl__set_prefix(PortableServer_Servant servant,
			     const CORBA_char * name, CORBA_Environment * ev);
static GBF_Target_FileList *impl__get_sources(PortableServer_Servant servant,
					      CORBA_Environment * ev);
static void impl_add_file (PortableServer_Servant servant,
			   const GBF_Target_FileList * file,
			   CORBA_Environment * ev);
static void impl_remove_file (PortableServer_Servant servant,
			      const GBF_Target_FileList * file,
			      CORBA_Environment * ev);
static Bonobo_stringlist *impl__get_groups (PortableServer_Servant servant,
					    CORBA_Environment * ev);
static GBF_Target_FileList *impl_get_group (PortableServer_Servant servant,
					    const CORBA_char * name,
					    CORBA_Environment * ev);
static void impl_set_group (PortableServer_Servant servant,
			    const CORBA_char * name,
			    const GBF_Target_FileList *files,
			    CORBA_Environment * ev);
static void impl_remove_group (PortableServer_Servant servant,
			       const CORBA_char * name, CORBA_Environment * ev);
static GBF_TargetList *impl__get_local_deps (PortableServer_Servant servant,
					     CORBA_Environment * ev);
static void impl_add_local_dependency (PortableServer_Servant servant,
				       const GBF_Target dep,
				       CORBA_Environment * ev);
static void impl_remove_local_dependency (PortableServer_Servant servant,
					  const GBF_Target dep,
					  CORBA_Environment * ev);
static Bonobo_stringlist *impl__get_dependencies(PortableServer_Servant servant,
					      CORBA_Environment * ev);
static void impl_add_dependency (PortableServer_Servant servant,
				 const Bonobo_stringlist * dep,
				 CORBA_Environment * ev);
static void impl_remove_dependency (PortableServer_Servant servant,
				    const Bonobo_stringlist * dep,
				    CORBA_Environment * ev);
static Bonobo_stringlist *impl__get_cflags (PortableServer_Servant servant,
					 CORBA_Environment * ev);
static void impl_add_cflag (PortableServer_Servant servant,
			    const Bonobo_stringlist * cflag,
			    CORBA_Environment * ev);
static void impl_remove_cflag (PortableServer_Servant servant,
			       const Bonobo_stringlist * cflag,
			       CORBA_Environment * ev);
static Bonobo_stringlist *impl__get_ldflags(PortableServer_Servant servant,
					 CORBA_Environment * ev);
static void impl_add_ldflag (PortableServer_Servant servant,
			     const Bonobo_stringlist * ldflag,
			     CORBA_Environment * ev);
static void impl_remove_ldflag (PortableServer_Servant servant,
				const Bonobo_stringlist * ldflag,
				CORBA_Environment * ev);

static POA_GBF_Target__vepv gbf_target_vepv;

GtkType gbf_target_get_type()
{
    static GtkType type = 0;

    if (!type) {
	GtkTypeInfo info = {
	    "IDL:GBF/Target:1.0",
	    sizeof(GbfTarget),
	    sizeof(GbfTargetClass),
	    (GtkClassInitFunc) target_class_init,
	    (GtkObjectInitFunc) target_init,
	    NULL, NULL,
	};
	type = gtk_type_unique(bonobo_object_get_type(), &info);
    }
    return type;
}

GbfTarget *gbf_target_new(char * name, char * type, char * prefix)
{
    GBF_Target corba_target;
    GbfTarget *target;

    /* the usual BonoboObject stuff */
    target = gtk_type_new(gbf_target_get_type());
    corba_target = (GBF_Target) create_gbf_target(BONOBO_OBJECT(target));
    if (corba_target == CORBA_OBJECT_NIL) {
	gtk_object_destroy(GTK_OBJECT(target));
	return NULL;
    }
    bonobo_object_construct(BONOBO_OBJECT(target), corba_target);

    target->name = g_strdup (name);
    target->prefix = g_strdup (prefix);
    /* FIXME: decide how to do the type */
    return target;
}

const char * gbf_target_get_name (GbfTarget * target)
{
    return target->name;
}

void gbf_target_add_file(GbfTarget * target, char * name, gint flags)
{
    char *str;
    gpointer storevalue;

    if (!target->sources) {
	target->sources = g_hash_table_new (g_str_hash, g_str_equal);
	/* FIXME: make sure this is the canonical name */
	str = g_strconcat (target->name, "_SOURCES", NULL);
	gbf_makefile_add_var (target->makefile, str, "SOURCES");
	g_free (str);
    }
    storevalue = GINT_TO_POINTER (flags);
    g_hash_table_insert (target->sources, g_strdup (name), storevalue);
}

void gbf_target_remove_file(GbfTarget * target, GSList * files)
{
}


void gbf_target_add_deps(GbfTarget * target, GSList * files)
{
    char *str;

    /* FIXME: probably need to copy every string too */
    target->deps = g_slist_copy (files);
    str = g_strconcat (target->name, "_DEPENDENCIES", NULL);
    gbf_makefile_add_var (target->makefile, str, "DEPS");
    g_free (str);
}

void gbf_target_remove_deps(GbfTarget * target, GSList * files)
{
}

void gbf_target_add_ldflags(GbfTarget * target, GSList * files)
{
    char *str;

    /* FIXME: probably need to copy every string too */
    target->deps = g_slist_copy (files);
    str = g_strconcat (target->name, "_LDFLAGS", NULL);
    gbf_makefile_add_var (target->makefile, str, "LDFLAGS");
    g_free (str);
}

void gbf_target_remove_ldflags(GbfTarget * target, GSList * files)
{
}

void gbf_target_add_libs(GbfTarget * target, GSList * files)
{
    char *str;

    /* FIXME: probably need to copy every string too */
    target->deps = g_slist_copy (files);
    str = g_strconcat (target->name, "_LIBRARIES", NULL);
    gbf_makefile_add_var (target->makefile, str, "LIBS");
    g_free (str);
}

void gbf_target_remove_libs(GbfTarget * target, GSList * files)
{
}

POA_GBF_Target__epv *gbf_target_get_epv(void)
{
    POA_GBF_Target__epv *epv;

    epv = g_new0(POA_GBF_Target__epv, 1);
    epv->_get_target_type = impl__get_target_type;
    epv->_get_name = impl__get_name;
    epv->_set_name = impl__set_name;
    epv->_get_prefix = impl__get_prefix;
    epv->_set_prefix = impl__set_prefix;
    epv->_get_sources = impl__get_sources;
    epv->add_file = impl_add_file;
    epv->remove_file = impl_remove_file;
    epv->_get_groups = impl__get_groups;
    epv->get_group = impl_get_group;
    epv->set_group = impl_set_group;
    epv->remove_group = impl_remove_group;
    epv->_get_local_deps = impl__get_local_deps;
    epv->add_local_dependency = impl_add_local_dependency;
    epv->remove_local_dependency = impl_remove_local_dependency;
    epv->_get_dependencies = impl__get_dependencies;
    epv->add_dependency = impl_add_dependency;
    epv->remove_dependency = impl_remove_dependency;
    epv->_get_cflags = impl__get_cflags;
    epv->add_cflag = impl_add_cflag;
    epv->remove_cflag = impl_remove_cflag;
    epv->_get_ldflags = impl__get_ldflags;
    epv->add_ldflag = impl_add_ldflag;
    epv->remove_ldflag = impl_remove_ldflag;

    return epv;
}

static void init_target_corba_class(void)
{
    gbf_target_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv();
    gbf_target_vepv.GBF_Target_epv = gbf_target_get_epv();
}

static CORBA_Object create_gbf_target(BonoboObject * object)
{
    POA_GBF_Target *servant;
    CORBA_Environment ev;

    CORBA_exception_init(&ev);
    servant = (POA_GBF_Target *) g_new0(GbfTarget, 1);
    servant->vepv = &gbf_target_vepv;

    POA_GBF_Target__init((PortableServer_Servant) servant, &ev);
    if (ev._major != CORBA_NO_EXCEPTION) {
	g_free(servant);
	CORBA_exception_free(&ev);
	return CORBA_OBJECT_NIL;
    }
    CORBA_exception_free(&ev);
    return bonobo_object_activate_servant(object, servant);
}

static void target_destroy(GtkObject * object)
{
}

static void target_class_init(GbfTargetClass * klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    gbf_target_parent_class = gtk_type_class(bonobo_object_get_type());

    object_class->destroy = target_destroy;

    init_target_corba_class();
}

static void target_init(GbfTarget * target)
{
    /* initialize to NULL so we can check when to add a SOURCES line
     * to the makefile
     */
    target->sources = NULL;
}

static GBF_Target_Type
impl__get_target_type(PortableServer_Servant servant, CORBA_Environment * ev)
{
   GBF_Target_Type retval = 0;

   return retval;
}

static CORBA_char *impl__get_name(PortableServer_Servant servant,
				  CORBA_Environment * ev)
{
   CORBA_char *retval = NULL;

   return retval;
}

static void
impl__set_name(PortableServer_Servant servant, const CORBA_char * name,
	       CORBA_Environment * ev)
{
}

static CORBA_char *impl__get_prefix(PortableServer_Servant servant,
				    CORBA_Environment * ev)
{
   CORBA_char *retval = NULL;

   return retval;
}

static void
impl__set_prefix(PortableServer_Servant servant, const CORBA_char * name,
		 CORBA_Environment * ev)
{
}

static GBF_Target_FileList *impl__get_sources(PortableServer_Servant servant,
					      CORBA_Environment * ev)
{
   GBF_Target_FileList *retval = NULL;

   return retval;
}

static void
impl_add_file (PortableServer_Servant servant,
	       const GBF_Target_FileList * file, CORBA_Environment * ev)
{
}

static void
impl_remove_file (PortableServer_Servant servant,
		  const GBF_Target_FileList * file, CORBA_Environment * ev)
{
}

static Bonobo_stringlist *
impl__get_groups (PortableServer_Servant servant,
		  CORBA_Environment * ev)
{
    Bonobo_stringlist *ret = NULL;

    return ret;
}

static GBF_Target_FileList *
impl_get_group (PortableServer_Servant servant,
		const CORBA_char * name,
		CORBA_Environment * ev)
{
    GBF_Target_FileList *ret = NULL;

    return ret;
}

static void
impl_set_group (PortableServer_Servant servant,
		const CORBA_char * name,
		const GBF_Target_FileList *files,
		CORBA_Environment * ev)
{
}
static void
impl_remove_group (PortableServer_Servant servant,
		   const CORBA_char * name, CORBA_Environment * ev)
{
}
static GBF_TargetList *
impl__get_local_deps (PortableServer_Servant servant, CORBA_Environment * ev)
{
   GBF_TargetList *retval = NULL;

   return retval;
}

static void
impl_add_local_dependency (PortableServer_Servant servant,
			   const GBF_Target dep, CORBA_Environment * ev)
{
}

static void
impl_remove_local_dependency (PortableServer_Servant servant,
			      const GBF_Target dep, CORBA_Environment * ev)
{
}

static Bonobo_stringlist *impl__get_dependencies(PortableServer_Servant servant,
					      CORBA_Environment * ev)
{
   Bonobo_stringlist *retval = NULL;

   return retval;
}

static void impl_add_dependency (PortableServer_Servant servant,
				 const Bonobo_stringlist * dep,
				 CORBA_Environment * ev)
{
}

static void impl_remove_dependency (PortableServer_Servant servant,
				    const Bonobo_stringlist * dep,
				    CORBA_Environment * ev)
{
}

static Bonobo_stringlist *
impl__get_cflags (PortableServer_Servant servant,
		  CORBA_Environment * ev)
{
   Bonobo_stringlist *retval = NULL;

   return retval;
}

static void
impl_add_cflag (PortableServer_Servant servant, const Bonobo_stringlist * cflag,
		CORBA_Environment * ev)
{
}

static void
impl_remove_cflag (PortableServer_Servant servant,
		   const Bonobo_stringlist * cflag, CORBA_Environment * ev)
{
}

static Bonobo_stringlist *impl__get_ldflags(PortableServer_Servant servant,
					 CORBA_Environment * ev)
{
   Bonobo_stringlist *retval = NULL;

   return retval;
}

static void
impl_add_ldflag (PortableServer_Servant servant, const Bonobo_stringlist * ldflag,
		 CORBA_Environment * ev)
{
}

static void
impl_remove_ldflag (PortableServer_Servant servant,
		    const Bonobo_stringlist * ldflag, CORBA_Environment * ev)
{
}

void
gbf_target_set_makefile (GbfTarget *target, GbfMakefile *makefile)
{
   target->makefile = makefile;
}
