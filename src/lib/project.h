#ifndef __PROJECT_H__
#define __PROJECT_H__

#include <bonobo/bonobo-object.h>
#include "types.h"
#include "gbf-project.h"
#include "makefile.h"
#include "line.h"

BEGIN_GNOME_DECLS


#define GBF_PROJECT_TYPE        (gbf_project_get_type ())
#define GBF_PROJECT(o)          (GTK_CHECK_CAST ((o), GBF_PROJECT_TYPE, GbfProject))
#define GBF_PROJECT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GBF_PROJECT_TYPE, GbfProjectClass))
#define GBF_IS_PROJECT(o)       (GTK_CHECK_TYPE ((o), GBF_PROJECT_TYPE))
#define GBF_IS_PROJECT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GBF_PROJECT_TYPE))

typedef enum {
    GBF_PITEM_PROJECT,
    GBF_PITEM_TARGET,
    GBF_PITEM_MAKEFILE,
    GBF_PITEM_FILE,
    GBF_PITEM_SUBST,
    GBF_PITEM_VAR
} GbfProjectItem;

struct _GbfProject {
    BonoboObject ancestor;

    gboolean changed;
    char *path;
    GHashTable *targets;
    GSList *makefiles;
    GSList *processed_files;
    GSList *subprojects;
    GHashTable *vars;
    GHashTable *automakevars;

    GbfProject *parent;
    GNode *lines;
    GNode *inserthere;
};

typedef struct {
    BonoboObjectClass parent_class;

    void (*added) (GbfProject * project,
		   GbfProjectItem what, gpointer item);

    void (*modified) (GbfProject * project,
		      GbfProjectItem what, gpointer item);

    void (*removed) (GbfProject * project,
		     GbfProjectItem what, gpointer item);

} GbfProjectClass;

GtkType gbf_project_get_type(void);
GbfProject *gbf_project_new(char * path);
char *gbf_project_get_path(GbfProject * project);

/* API for parser */
void gbf_project_add_child(GbfProject * project, GbfProject * child);
void gbf_project_add_makefile(GbfProject * project,
			      GbfMakefile * makefile);
void gbf_project_add_file(GbfProject * project, char * file);

void gbf_project_add_subst(GbfProject * project, char * name,
			   char * value);
GbfLine *gbf_project_get_subst(GbfProject * project, char * name);

void gbf_project_add_var(GbfProject * project, char * name,
			 char * value);
GbfLine *gbf_project_get_var(GbfProject * project, char * name);

POA_GBF_Project__epv *gbf_project_get_epv(void);


END_GNOME_DECLS
#endif				/* __PROJECT_H__ */
