#ifndef __PARSER_CLIENT_H__
#define __PARSER_CLIENT_H__

BEGIN_GNOME_DECLS

#define GBF_TYPE_PARSER_CLIENT            (gbf_parser_client_get_type ())
#define GBF_PARSER_CLIENT(obj)            (GTK_CHECK_CAST ((obj), GBF_TYPE_PARSER_CLIENT, GbfParserClient))
#define GBF_PARSER_CLIENT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBF_TYPE_PARSER_CLIENT, GbfParserClient))
#define GBF_IS_PARSER_CLIENT(obj)         (GTK_CHECK_TYPE ((obj), GBF_TYPE_PARSER_CLIENT))
#define GBF_IS_PARSER_CLIENT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBF_TYPE_PARSER_CLIENT))

struct _GbfParserClient {
    GtkObject ancestor;

    GbfParser *parser;

    GHashTable *file_handlers;
    GSList *projects;
    GSList *libraries;
};

typedef struct _GbfParserClientClass {
    GtkObjectClass parent;

    /* virtual */
    void     (*parser_warning)        (GbfParserClient *client, char *warning);
    gboolean (*parser_question_yesno) (GbfParserClient *client, char *msg);
    void     (*add_project)           (GbfParserClient *client,
				       GbfProject      *project);
    void     (*add_library)           (GbfParserClient *client,
				       char            *label,
				       GbfLibrary      *library);
} GbfParserClientClass;


GtkType gbf_parser_client_get_type       (void);


/* parser API */
void     gbf_parser_client_init           (void);
void     gbf_parser_client_warning        (GbfParserClient *client,
					   char            *warning);
gboolean gbf_parser_client_question_yesno (GbfParserClient *client,
					   char            *msg);
void     gbf_parser_client_add_project    (GbfParserClient *client,
					   GbfProject      *project,
					   GbfProject      *parent);
void     gbf_parser_client_add_library    (GbfParserClient *client,
					   char            *label,
					   GbfLibrary      *library);


END_GNOME_DECLS
#endif				/* __PARSER_CLIENT_H__ */
