#ifndef __MAKEFILE_H__
#define __MAKEFILE_H__

#include "types.h"
#include "target.h"

BEGIN_GNOME_DECLS

#define GBF_TYPE_MAKEFILE            (gbf_makefile_get_type ())
#define GBF_MAKEFILE(obj)            (GTK_CHECK_CAST ((obj), GBF_TYPE_MAKEFILE, GbfMakefile))
#define GBF_MAKEFILE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBF_TYPE_MAKEFILE, GbfMakefileClass))
#define GBF_IS_MAKEFILE(obj)         (GTK_CHECK_TYPE ((obj), GBF_TYPE_MAKEFILE))
#define GBF_IS_MAKEFILE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBF_TYPE_MAKEFILE))

struct _GbfMakefile {
    GtkObject ancestor;

    gboolean changed;
    char *path;
    GHashTable *targets;
    GHashTable *vars;
    GHashTable *rules;

    GNode *lines;
    GNode *inserthere;
};


GtkType gbf_makefile_get_type();

GbfMakefile *gbf_makefile_new(char * path);
char *gbf_makefile_get_path(GbfMakefile * makefile);
void gbf_makefile_add_target(GbfMakefile * makefile, GbfTarget * target);
void gbf_makefile_add_var(GbfMakefile * makefile,
			  char * name, char * value);
void gbf_makefile_add_rule (GbfMakefile * makefile,
			    char * name, char * value);


END_GNOME_DECLS
#endif				/* __MAKEFILE_H__ */
