#ifndef __PARSER_H__
#define __PARSER_H__

BEGIN_GNOME_DECLS


#define GBF_TYPE_PARSER            (gbf_parser_get_type ())
#define GBF_PARSER(obj)            (GTK_CHECK_CAST ((obj), GBF_TYPE_PARSER, GbfParser))
#define GBF_PARSER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBF_TYPE_PARSER, GbfParser))
#define GBF_IS_PARSER(obj)         (GTK_CHECK_TYPE ((obj), GBF_TYPE_PARSER))
#define GBF_IS_PARSER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBF_TYPE_PARSER))

struct _GbfParser {
    GtkObject ancestor;

    GbfParserClient *client;
};

typedef struct _GbfParserClass {
    GtkObjectClass parent;

    /* virtual methods */
    gboolean (*read)      (GbfParser *parser, char *path);
    gboolean (*write)     (GbfParser *parser, GSList *projects);
    void     (*find_libs) (GbfParser *parser, char *language);
} GbfParserClass;

GtkType    gbf_parser_get_type (void);

gboolean gbf_parser_read      (GbfParser *parser,
			       char      *path);
gboolean gbf_parser_write     (GbfParser *parser,
			       GSlist    *projects);
void     gbf_parser_find_libs (GbfParser *parser,
			       char      *language);


END_GNOME_DECLS
#endif				/* __PARSER_H__ */
