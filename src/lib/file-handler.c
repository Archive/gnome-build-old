#include <gtk/gtk.h>
#include <bonobo.h>
#include "file-handler.h"

enum {
	ON_GETVERB,
	ON_DOVERB,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL];
static GtkObjectClass *parent_class;

static CORBA_Object create_gbf_file_handler (BonoboObject * object);
static void file_handler_destroy (GtkObject * object);
static void file_handler_class_init (GbfFileHandlerClass * class);
static void file_handler_init (GbfFileHandler * file_handler);
static Bonobo_stringlist *impl_get_verb_list (PortableServer_Servant _servant,
					      const CORBA_char * path,
					      CORBA_Environment * ev);
static void impl_do_verb (PortableServer_Servant _servant,
			  const CORBA_char * path,
			  const CORBA_char * verb,
			  CORBA_Environment *ev);
static void impl_unregistered (PortableServer_Servant servant,
			       CORBA_Environment * ev);


static POA_GBF_FileHandler__vepv gbf_file_handler_vepv;

GtkType
gbf_file_handler_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"IDL:GBF/FileHandler:1.0",
			sizeof (GbfFileHandler),
			sizeof (GbfFileHandlerClass),
			(GtkClassInitFunc) file_handler_class_init,
			(GtkObjectInitFunc) file_handler_init,
			NULL, NULL,
		};
		type = gtk_type_unique (bonobo_object_get_type (), &info);
	}
	return type;
}

GbfFileHandler *
gbf_file_handler_new (BonoboUIHandler * handler)
{
	GBF_FileHandler corba_file_handler;
	GbfFileHandler *file_handler;

	file_handler = gtk_type_new (gbf_file_handler_get_type ());
	corba_file_handler = (GBF_FileHandler) create_gbf_file_handler (BONOBO_OBJECT (file_handler));
	if (corba_file_handler == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (file_handler));
		return NULL;
	}
	bonobo_object_construct (BONOBO_OBJECT (file_handler), corba_file_handler);

	return file_handler;
}

POA_GBF_FileHandler__epv *gbf_file_handler_get_epv (void)
{
    POA_GBF_FileHandler__epv *epv;

    epv = g_new0 (POA_GBF_FileHandler__epv, 1);
    epv->get_verb_list = impl_get_verb_list;
    epv->do_verb = impl_do_verb;
    epv->unregistered = impl_unregistered;

    return epv;
}

static void init_file_handler_corba_class (void)
{
    gbf_file_handler_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
    gbf_file_handler_vepv.GBF_FileHandler_epv = gbf_file_handler_get_epv ();
}

static Bonobo_stringlist *
impl_get_verb_list (PortableServer_Servant servant, const CORBA_char * path,
                    CORBA_Environment      * ev)
{
    Bonobo_stringlist *ret = NULL;

    return ret;
}

static void
impl_do_verb (PortableServer_Servant servant, const CORBA_char * file,
              const CORBA_char * verb, CORBA_Environment * ev)
{
}

static void
impl_unregistered (PortableServer_Servant servant, CORBA_Environment * ev)
{
}

static CORBA_Object
create_gbf_file_handler (BonoboObject * object)
{
	POA_GBF_FileHandler *servant;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	servant = (POA_GBF_FileHandler *)g_new0 (GbfFileHandler, 1);
	servant->vepv = &gbf_file_handler_vepv;

	POA_GBF_FileHandler__init ((PortableServer_Servant) servant, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);
	return bonobo_object_activate_servant (object, servant);
}

static void
file_handler_destroy (GtkObject * object)
{
	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

typedef void (*GtkSignal_NONE__STRING_POINTER) (GtkObject * object,
						gchar * arg1,
						gpointer arg2,
						gpointer user_data);
static void
gtk_marshal_NONE__STRING_POINTER (GtkObject * object,
				  GtkSignalFunc func,
				  gpointer func_data,
				  GtkArg * args)
{
    GtkSignal_NONE__STRING_POINTER rfunc;
    rfunc = (GtkSignal_NONE__STRING_POINTER) func;

    (*rfunc) (object, GTK_VALUE_STRING (args[0]),
	      GTK_VALUE_POINTER (args[1]),
	      func_data);
}

typedef void (*GtkSignal_NONE__STRING_STRING_POINTER) (GtkObject * object,
						       gchar * arg1,
						       gchar * arg2,
						       gpointer arg3,
						       gpointer user_data);
static void
gtk_marshal_NONE__STRING_STRING_POINTER (GtkObject * object,
					 GtkSignalFunc func,
					 gpointer func_data,
					 GtkArg * args)
{
    GtkSignal_NONE__STRING_STRING_POINTER rfunc;
    rfunc = (GtkSignal_NONE__STRING_STRING_POINTER) func;

    (*rfunc) (object, GTK_VALUE_STRING (args[0]),
	      GTK_VALUE_STRING (args[1]),
	      GTK_VALUE_POINTER (args[2]),
	      func_data);
}

static void
file_handler_class_init (GbfFileHandlerClass * klass)
{
    GtkObjectClass *object_class = (GtkObjectClass *) klass;

    parent_class =
	gtk_type_class (bonobo_object_get_type ());

    signals[ON_GETVERB] =
	gtk_signal_new ("on_getverb",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (GbfFileHandlerClass, on_getverb),
			gtk_marshal_NONE__STRING_POINTER,
			GTK_TYPE_NONE, 2,
			GTK_TYPE_STRING, GTK_TYPE_POINTER);
    signals[ON_DOVERB] = 
	gtk_signal_new ("on_doverb",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (GbfFileHandlerClass, on_doverb),
			gtk_marshal_NONE__STRING_STRING_POINTER,
			GTK_TYPE_NONE, 3,
			GTK_TYPE_STRING, GTK_TYPE_STRING, GTK_TYPE_POINTER);

    gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
    object_class->destroy = file_handler_destroy;
    init_file_handler_corba_class ();
}

static void
file_handler_init (GbfFileHandler * file_handler)
{
}
