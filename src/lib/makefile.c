#include "makefile.h"
#include "line.h"

GbfMakefile *gbf_makefile_new(char * path)
{
    GbfMakefile *new;

    new = g_new0(GbfMakefile, 1);
    new->path = g_strdup(path);
    new->targets = g_hash_table_new(g_str_hash, g_str_equal);
    new->vars = g_hash_table_new(g_str_hash, g_str_equal);
    new->lines = g_node_new(NULL);

    return new;
}

void gbf_makefile_add_target(GbfMakefile * makefile, GbfTarget * target)
{
    GbfLine *current, *parent;
    
    current = makefile->inserthere->data;

    if (current->lt == GBF_LINE_TARGET) {
	parent = makefile->inserthere->parent->data;
	if (parent->lt != GBF_LINE_TARGETLIST) {
	    g_error ("eek!");
	}
	if ((target->type == GPOINTER_TO_INT (current->value)) &&
	    (!strcmp (target->prefix, current->name))) {
	    /* add to parent */
	} else {
	    /* create new target list
	     * add target to list
	     */
	}
    } else {
	/* create new target list
	 * add target to list
	 */
    }

	
    g_hash_table_insert(makefile->targets, 
			(char *)gbf_target_get_name(target),
			makefile->inserthere);
    gbf_target_set_makefile (target, makefile);
}

void
gbf_makefile_add_var(GbfMakefile * makefile, char * name, char * value)
{
    char *storename, *storevalue;
    gpointer a, b;

    storename = g_strdup(name);
    storevalue = g_strdup(value);

    makefile->inserthere = gbf_line_create(makefile->inserthere,
					   GBF_LINE_VAR,
					   storename, storevalue);
    if (!g_hash_table_lookup_extended(makefile->vars, name, &a, &b)) {
	g_hash_table_insert(makefile->vars, storename,
			    makefile->inserthere);
    }
}
