# crap to simulate target storage so I can test
package Gnome::Build::Target;

use strict;
use Gnome::Build::Util ('canonicalize');

sub new ($$$) {
	my ($object, $name, $type, $prefix) = @_;
	my $target = {
		"name"	=> $name,
		"type"	=> $type,
		"prefix"	=> $prefix,
	};
#	bless $target, 'Gnome::Build::Target';
	bless $target, $object;
	print "new target $name ($type, $prefix)\n";
	return $target;
}

sub set_name {
	my ($target, $name) = @_;

	$target->{"name"} = $name;
}

sub get_name {
	my $target = shift(@_);

	return $target->{"name"};
}

sub set_type {
	my ($target, $type) = @_;

	$target->{"type"} = $type;
}

sub get_type {
	my $target = shift(@_);

	return $target->{"type"};
}

sub set_prefix {
	my ($target, $prefix) = @_;

	$target->{"prefix"} = $prefix;
}

sub get_prefix {
	my $target = shift(@_);

	return $target->{"prefix"};
}

sub add_file {
	my ($target, $name, $ffref, $ws) = @_;

	print "adding file ($name\[$ws])\n";
}

sub set_sources {
	my ($target, $sources) = @_;
	my (%hash, $file);

	foreach (split (/\s+/, $sources)) {
		$hash{$file} = "";
	}
	$target->{"sources"} = \%hash;
}

sub set_file_flags {
	my ($target, $file, $flags) = @_;

	my ($hashref) = $target->{"sources"};
	$$hashref{$file} = $flags;
}

sub get_sources {
	my $target = shift(@_);

	return $target->{"sources"};
}

sub set_dependencies {
	my ($target, $dependencies) = @_;

	$target->{"dependencies"} = $dependencies;
}

sub get_dependencies {
	my $target = shift(@_);
	my $ref;

	$ref = $target->{"dependencies"};
	return @$ref if ($ref);
	return ();
}

sub set_ldadd {
	my ($target, $ldadd) = @_;

	$target->{"ldadd"} = $ldadd;
}

sub get_ldadd {
	my $target = shift(@_);
	my $ref;

	$ref = $target->{"ldadd"};
	return ($ref? @$ref : ());
}

sub set_ldflags {
	my ($target, $ldflags) = @_;

	$target->{"ldflags"} = $ldflags;
}

sub get_ldflags {
	my $target = shift(@_);
	my $ref;

	$ref = $target->{"ldflags"};
	return @$ref if ($ref);
	return ();
}

# FIXME: return ref instead of list?
sub write {
	my ($target, $prefix) = @_;
	my ($arr_ref, $hash_ref, $file, @result);

	$hash_ref = $target->{"sources"};
	if ($hash_ref) {
		if ($target->{"name"} eq "aux") {
			my $str;
			$str = $target->get_type();
			$str =~ tr[a-z][A-Z];
			push (@result, $target->{"prefix"} . "_$str = \\\n");
		} else {
			push (@result, canonicalize($target->{"name"}) . "_SOURCES = \\\n");
		}
		foreach $file (keys (%$hash_ref)) {
			push (@result, "\t$file \\\n");
		}
		$result[$#result] =~ s/\s*\\$/\n/;
		return (@result) if ($target->{"name"} eq "aux");
	}
	$arr_ref = $target->{"dependencies"};
	if ($arr_ref != undef) {
		push (@result, canonicalize($target->{"name"}) . "_DEPENDENCIES = \\\n");
		foreach $file (@$arr_ref) {
			push (@result, "\t$file \\\n");
		}
		$result[$#result] =~ s/\s*\\$/\n/;
	}
	$arr_ref = $target->{"ldflags"};
	if ($arr_ref != undef) {
		push (@result, canonicalize($target->{"name"}) . "_LDFLAGS = \\\n");
		foreach $file (@$arr_ref) {
			push (@result, "\t$file \\\n");
		}
		$result[$#result] =~ s/\s*\\$/\n/;
	}
	$arr_ref = $target->{"ldadd"};
	if ($arr_ref != undef) {
		my $str;
		if (($target->{"type"} eq "LIBRARIES")
		|| ($target->{"type"} eq "LTLIBRARIES")) {
			$str = "_LIBADD";
		} else {
			$str = "_LDADD";
		}
		push (@result, canonicalize($target->{"name"}) . $str . " = \\\n");
		foreach $file (@$arr_ref) {
			push (@result, "\t$file \\\n");
		}
		$result[$#result] =~ s/\s*\\$/\n/;
	}
	return @result;
}

1;
