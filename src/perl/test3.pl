use CPAN;

for $mod (CPAN::Shell->expand("Module","/./")){
	next unless $mod->inst_file;
	# MakeMaker convention for undefined $VERSION:
#	next unless $mod->inst_version eq "undef";
	if ($mod->cpan_file !~ /perl-[\d\.]+\.tar\.gz/) {
		print $mod->id, "---", $mod->cpan_file, "\n";
	}
}

